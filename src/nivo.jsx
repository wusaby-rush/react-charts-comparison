import React from 'react'
import ReactDOM from 'react-dom/client'
import data from '../store/nivo-data'
import { ResponsiveLine } from '@nivo/line'
import { ResponsiveStream } from '@nivo/stream'

function MyLineChart() {
  return (
    <div style={{ width: '1200px', height: '800px' }}>
        <ResponsiveLine
        data={data.line}
        margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
        xScale={{ type: 'point' }}
        yScale={{
            type: 'linear',
            min: -95,
            max: 'auto',
            stacked: true,
            reverse: false
        }}
        yFormat=" >-.2f"
        colors={{ scheme: 'dark2' }}
        pointSize={0}
        enableArea={true}
        areaOpacity={0.1}
        useMesh={true}
        isInteractive={true}
        enableSlices={false}
        debugSlices={true}
        legends={[
            {
                anchor: 'bottom-right',
                direction: 'column',
                justify: false,
                translateX: 100,
                translateY: 0,
                itemsSpacing: 0,
                itemDirection: 'left-to-right',
                itemWidth: 80,
                itemHeight: 20,
                itemOpacity: 0.75,
                symbolSize: 12,
                symbolShape: 'circle',
                symbolBorderColor: 'rgba(0, 0, 0, .5)',
                effects: [
                    {
                        on: 'hover',
                        style: {
                            itemBackground: 'rgba(0, 0, 0, .03)',
                            itemOpacity: 1
                        }
                    }
                ]
            }
        ]}
    />
    </div>
  )
}

function MyAreaChart() {
  return (
    <div style={{ width: '1200px', height: '800px' }} > 
        <ResponsiveStream
            data={data.area}
            keys={[
                'Raoul',
                'Josiane',
                'Marcel',
                'René',
                'Paul',
                'Jacques'
            ]}
            margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
            axisTop={null}
            axisRight={null}
            axisBottom={{
                orient: 'bottom',
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: '',
                legendOffset: 36
            }}
            axisLeft={{
                orient: 'left',
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: '',
                legendOffset: -40
            }}
            enableGridX={true}
            enableGridY={false}
            offsetType="silhouette"
            colors={{ scheme: 'nivo' }}
            fillOpacity={0.85}
            borderColor={{ theme: 'background' }}
            defs={[
                {
                    id: 'dots',
                    type: 'patternDots',
                    background: 'inherit',
                    color: '#2c998f',
                    size: 4,
                    padding: 2,
                    stagger: true
                },
                {
                    id: 'squares',
                    type: 'patternSquares',
                    background: 'inherit',
                    color: '#e4c912',
                    size: 6,
                    padding: 2,
                    stagger: true
                }
            ]}
            fill={[
                {
                    match: {
                        id: 'Paul'
                    },
                    id: 'dots'
                },
                {
                    match: {
                        id: 'Marcel'
                    },
                    id: 'squares'
                }
            ]}
            dotSize={8}
            dotColor={{ from: 'color' }}
            dotBorderWidth={2}
            dotBorderColor={{
                from: 'color',
                modifiers: [
                    [
                        'darker',
                        0.7
                    ]
                ]
            }}
            legends={[
                {
                    anchor: 'bottom-right',
                    direction: 'column',
                    translateX: 100,
                    itemWidth: 80,
                    itemHeight: 20,
                    itemTextColor: '#999999',
                    symbolSize: 12,
                    symbolShape: 'circle',
                    effects: [
                        {
                            on: 'hover',
                            style: {
                                itemTextColor: '#000000'
                            }
                        }
                    ]
                }
            ]}
        />
    </div>
  )
}

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <>
      <MyLineChart />
      <MyAreaChart />
    </>
  </React.StrictMode>,
)
