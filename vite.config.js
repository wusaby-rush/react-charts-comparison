import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import { resolve } from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, 'index.html'),
        recharts: resolve(__dirname, 'recharts.html'),
        nivo: resolve(__dirname, 'nivo.html'),
      },
    },
  },
  plugins: [react()],
})
