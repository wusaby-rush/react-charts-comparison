const line = [
    {
      "id": "japan",
      "color": "hsl(50, 70%, 50%)",
      "data": [
        {
          "x": "plane",
          "y": 290
        },
        {
          "x": "helicopter",
          "y": 245
        },
        {
          "x": "boat",
          "y": 64
        },
        {
          "x": "train",
          "y": 186
        },
        {
          "x": "subway",
          "y": 174
        },
        {
          "x": "bus",
          "y": 213
        },
        {
          "x": "car",
          "y": 177
        },
        {
          "x": "moto",
          "y": 36
        },
        {
          "x": "bicycle",
          "y": 126
        },
        {
          "x": "horse",
          "y": 101
        },
        {
          "x": "skateboard",
          "y": 78
        },
        {
          "x": "others",
          "y": 234
        }
      ]
    },
    {
      "id": "france",
      "color": "hsl(30, 70%, 50%)",
      "data": [
        {
          "x": "plane",
          "y": 214
        },
        {
          "x": "helicopter",
          "y": 163
        },
        {
          "x": "boat",
          "y": 196
        },
        {
          "x": "train",
          "y": 286
        },
        {
          "x": "subway",
          "y": 144
        },
        {
          "x": "bus",
          "y": 120
        },
        {
          "x": "car",
          "y": 256
        },
        {
          "x": "moto",
          "y": 187
        },
        {
          "x": "bicycle",
          "y": 41
        },
        {
          "x": "horse",
          "y": 111
        },
        {
          "x": "skateboard",
          "y": 251
        },
        {
          "x": "others",
          "y": 247
        }
      ]
    },
    {
      "id": "us",
      "color": "hsl(196, 70%, 50%)",
      "data": [
        {
          "x": "plane",
          "y": 31
        },
        {
          "x": "helicopter",
          "y": 217
        },
        {
          "x": "boat",
          "y": 292
        },
        {
          "x": "train",
          "y": 22
        },
        {
          "x": "subway",
          "y": 58
        },
        {
          "x": "bus",
          "y": 116
        },
        {
          "x": "car",
          "y": 75
        },
        {
          "x": "moto",
          "y": 154
        },
        {
          "x": "bicycle",
          "y": 249
        },
        {
          "x": "horse",
          "y": 187
        },
        {
          "x": "skateboard",
          "y": 296
        },
        {
          "x": "others",
          "y": 261
        }
      ]
    },
    {
      "id": "germany",
      "color": "hsl(93, 70%, 50%)",
      "data": [
        {
          "x": "plane",
          "y": 68
        },
        {
          "x": "helicopter",
          "y": 202
        },
        {
          "x": "boat",
          "y": 222
        },
        {
          "x": "train",
          "y": 37
        },
        {
          "x": "subway",
          "y": 278
        },
        {
          "x": "bus",
          "y": 113
        },
        {
          "x": "car",
          "y": 203
        },
        {
          "x": "moto",
          "y": 19
        },
        {
          "x": "bicycle",
          "y": 4
        },
        {
          "x": "horse",
          "y": 34
        },
        {
          "x": "skateboard",
          "y": 146
        },
        {
          "x": "others",
          "y": 212
        }
      ]
    },
    {
      "id": "norway",
      "color": "hsl(94, 70%, 50%)",
      "data": [
        {
          "x": "plane",
          "y": 266
        },
        {
          "x": "helicopter",
          "y": 149
        },
        {
          "x": "boat",
          "y": 51
        },
        {
          "x": "train",
          "y": 276
        },
        {
          "x": "subway",
          "y": 107
        },
        {
          "x": "bus",
          "y": 299
        },
        {
          "x": "car",
          "y": 34
        },
        {
          "x": "moto",
          "y": 280
        },
        {
          "x": "bicycle",
          "y": 155
        },
        {
          "x": "horse",
          "y": 81
        },
        {
          "x": "skateboard",
          "y": 217
        },
        {
          "x": "others",
          "y": 121
        }
      ]
    }
]

const area = [
    {
      "Raoul": 114,
      "Josiane": 136,
      "Marcel": 84,
      "René": 156,
      "Paul": 65,
      "Jacques": 59
    },
    {
      "Raoul": 99,
      "Josiane": 53,
      "Marcel": 42,
      "René": 133,
      "Paul": 170,
      "Jacques": 46
    },
    {
      "Raoul": 111,
      "Josiane": 186,
      "Marcel": 27,
      "René": 184,
      "Paul": 87,
      "Jacques": 138
    },
    {
      "Raoul": 148,
      "Josiane": 189,
      "Marcel": 100,
      "René": 122,
      "Paul": 172,
      "Jacques": 188
    },
    {
      "Raoul": 141,
      "Josiane": 57,
      "Marcel": 80,
      "René": 42,
      "Paul": 87,
      "Jacques": 182
    },
    {
      "Raoul": 66,
      "Josiane": 148,
      "Marcel": 133,
      "René": 183,
      "Paul": 62,
      "Jacques": 70
    },
    {
      "Raoul": 12,
      "Josiane": 197,
      "Marcel": 17,
      "René": 145,
      "Paul": 17,
      "Jacques": 22
    },
    {
      "Raoul": 82,
      "Josiane": 103,
      "Marcel": 161,
      "René": 170,
      "Paul": 110,
      "Jacques": 196
    },
    {
      "Raoul": 145,
      "Josiane": 81,
      "Marcel": 168,
      "René": 170,
      "Paul": 39,
      "Jacques": 74
    }
]

const data = { line, area }
export default data